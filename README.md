<div align="center">
  <img width="200" height="200" src="https://webpack.js.org/assets/icon-square-big.svg">
  <h1>Шаблон веб-пакета</h1>
  <p>
  Webpack - это модуль-упаковщик. Его основная цель - связывать файлы JavaScript для использования в браузере, но он также способен преобразовывать, связывать или упаковывать практически любой ресурс или ресурс.
  </p>
  <p>Author: <a href="https://tocode.ru" target="_blank">To code</a> | <a href="https://www.youtube.com/playlist?list=PLkCrmfIT6LBQWN02hNj6r1daz7965GxsV" target="_blank">Youtube guide in Russian</a></p>
</div>


## Настройка сборки:

``` bash
# Скачать репозиторий:
git clone https://github.com/vedees/webpack-template webpack-template

# Перейти в приложение: 
cd webpack-template

# Установить зависимости:
npm install

# Сервер с горячей перезагрузкой по адресу http: // localhost: 8081 /
npm run dev

# Вывод будет в папке dist / 
npm run build
```

## Структура проекта:

* `src/index.html` - основное приложение HTML
* `src/assets/scss` - поставить пользовательские стили приложения SCSS здесь. Не забудьте импортировать их в `index.js`
* `src/assets/css` - так же, как и выше, но CSS здесь. Не забудьте импортировать их в `index.js`
* `src/assets/img` - положить изображения здесь. Не забудьте использовать правильный путь: `assets/img/some.jpg`
* `src/js` - поместите скрипты пользовательских приложений здесь
* `src/index.js`  - основной файл приложения, в который вы включаете / импортируете все необходимые библиотеки и приложение init
* `src/components` - папка с пользовательскими `.vue` компонентами
* `src/store` - магазин приложений для vue
* `static/` - папка с дополнительными статическими ресурсами, которые будут скопированы в выходную папку

<div align="center">
  <h2>Настройки:</h2>
</div>

## Основное const:
Простой способ переместить все файлы.
По умолчанию:
``` js
const PATHS = {
  // Путь к основному приложению dir 
  src: path.join(__dirname, '../src'),
 // Путь к выводу dir 
  dist: path.join(__dirname, '../dist'),
  // Путь ко второму выходному каталогу dir (папка js / css / fonts и т. д.) 
  assets: 'assets/'
}
```
## Настройка:
Измените любые папки:
``` js
const PATHS = {
  // src должен быть src 
  src: path.join(__dirname, '../src'),
  // dist to public 
  dist: path.join(__dirname, '../public'),
  // ресурсы к статическим 
  assets: 'static/'
}
```

## Импорт других библиотек:
1. Установить библиотеки
2. Импорт библиотек в `./index.js`
``` js
// Реагируем на пример 
import React from 'react'
// Пример Bootstrap 
import Bootstrap from 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/js/bootstrap.min.js'
```

## Импортируйте только библиотеки SASS или CSS:
1. Установить библиотеки
2. Перейти к `/assets/scss/utils/libs.scss`
3. Импорт библиотек в модулях узлов
``` scss
// Пример библиотеки Sass:
@import '../../node_modules/spinners/stylesheets/spinners';
// Пример библиотек CSS: 
@import '../../node_modules/flickity/dist/flickity.css';
```

## Импорт JS-файлов:
1. Создайте еще один модуль js в `./js/` папке
2. Импортировать модули в `./js/index.js` файл
``` js
// another js file for example
import './common.js'
```

## Папка HTML Dir:
#### По умолчанию:
* .html dir: `./src`
* Конфигурации: в  `./build/webpack.base.conf.js`
``` js
const PAGES_DIR = PATHS.src
```
**Использование:**
Все файлы должны быть созданы в `./src` папке.
Пример:
``` bash
./src/index.html
./src/about.html
```

#### Изменить папку по умолчанию для HTML:
Пример для `./src/pages`:
1. Создать папку для всех файлов HTML в `./src`. Быть как: `./src/pages`
2. Изменить  `./build/webpack.base.conf.js` const PAGES_DIR:
``` js
// Старый путь
// const PAGES_DIR = PATHS.src

// Ваш новый путь 
const PAGES_DIR = `${PATHS.src}/pages`
```
3. Rerun webpack dev server


**Использование:**
Все файлы должны быть созданы в `./src/pages` папке.
Пример:
``` bash
./src/pages/index.html
./src/pages/about.html
```

## Создайте еще один HTML-файл:
#### По умолчанию: 
Автоматическое создание любых html-страниц:
1. Создать еще один HTML-файл в `./src` (основная папка)
2. Открыть новую страницу `http://localhost:8081/about.html` (Не забудьте сервер Dev Rerun) Видеть больше 
- совершить - [совершить](https://github.com/vedees/webpack-template/commit/249e3ae3b4973a7300f271045178f9f5f431bb89)

#### Второй метод:
Ручное (не автоматическое) создание любых html-страниц (не забудьте RERUN dev server и строки COMMENT выше)
1. Создать еще один HTML-файл в `./src` (основная папка)
2. Перейти к `./build/webpack.base.conf.js`
3. Команды выше (создайте автоматически HTML-страницы)
4. Создать новую страницу в формате HTML:
``` js
    new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/index.html`,
      filename: './index.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/another.html`,
      filename: './another.html',
      inject: true
    }),
```
5. Открыть новую страницу `http://localhost:8081/about.html` (не забудьте RERUN dev server)

#### Третий метод: (ЛУЧШИЙ)
Комбинируйте первый метод и второй.
Пример:
``` js
    ...PAGES.map(page => new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/${page}`,
      filename: `./${page}`
    })),
    new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/about/index.html`,
      filename: './about/index.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/about/portfolio.html`,
      filename: './about/portfolio.html',
      inject: true
    }),
```


## Vue установить:
По умолчанию: **уже есть**

1. Установить vue
``` bash
npm install vue --save
```
2. Init vue `index.js`:
``` js
const app = new Vue({
  el: '#app'
})
```
3. Создать приложение Div ID
``` html
<div id="app">
  <!-- content -->
</div>
```

## Vuex установить:
1. Установить Vuex
``` bash
npm install vuex --save
```
2. Импорт Vuex
``` js
import store from './store'
```
3. Создать  index.js в `./store`
``` js
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  // vuex content
})
```

## Добавить Vue Компоненты:
Создайте свой компонент в `/components/`

**Использование HTML:**
1. Начальный компонент в `index.js`:
``` js
Vue.component('example-component', require('./components/Example.vue').default)
```
2. Любые HTML-файлы:
``` html
 <example-component />
```

**VUE Использование:**
1. импортировать компоненты в .vue:
``` js
import example from '~/components/Example.vue'
```
2. Зарегистрировать компонент:
``` js
  components: {
    example
  }
```
3. Init в vue компонент:
``` html
<example />
```

## Добавить шрифты:
Добавьте @font-face in `/assets/scss/utils/fonts.scss`:

``` scss
// Пример с Helvetica 
@font-face {
  font-family: "Helvetica-Base";
  src: url('/assets/fonts/Helvetica/Base/Helvetica-Base.eot'); /* Режимы сравнения IE9 */
  src: url('/assets/fonts/Helvetica/Base/Helvetica-Base.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/assets/fonts/Helvetica/Base/Helvetica-Base.woff') format('woff'), /* Довольно современные браузеры */
       url('/assets/fonts/Helvetica/Base/Helvetica-Base.ttf')  format('truetype'), /* Safari, Android, iOS */
       url('/assets/fonts/Helvetica/Base/Helvetica-Base.svg') format('svg'); /* Legacy iOS */
}
```

Добавьте переменные для шрифта в `/assets/scss/utils/vars.scss`:

``` scss
$mySecontFont : 'Helvetica-Base', Arial, sans-serif;
```


## Лицензия
[MIT](./LICENSE)

Copyright (c) 2018-present, [Evgenii Vedegis](https://github.com/vedees)